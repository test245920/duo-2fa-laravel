<?php

return [
    'client_id' => env('DUO_CLIENT_ID'),
    'client_secret' => env('DUO_CLIENT_SECRET'),
    'api_hostname' => env('DUO_API_HOST'),
    'redirect_uri' => env('DUO_API_REDIRECT_HOST'),
//    'http_proxy' => env('DUO_API_HOST')
];
