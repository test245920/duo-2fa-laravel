<?php

use App\Http\Controllers\DuoController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group([
    'as' => 'duo.',
    'prefix' => '/duo',
], function () {
    Route::get('/form', [DuoController::class, 'show'])
        ->middleware('auth')
        ->name('form');
    Route::get('/verify', [DuoController::class, 'verifyUser'])
        ->name('verify');
});
