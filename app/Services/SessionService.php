<?php

namespace App\Services;

class SessionService
{
    public static function set(string $key, mixed $value): void
    {
        session()->put($key, $value);
    }

    public static function get(string $key): mixed
    {
        return session()->get($key);
    }

    public static function clean(): void
    {
        session()->flush();
    }

    public static function has(string $key): bool
    {
        return !!self::get($key);
    }
}
