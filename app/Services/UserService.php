<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    public function findUserByEmail(string $email): ?User
    {
        return User::whereEmail($email)->first();
    }
}
