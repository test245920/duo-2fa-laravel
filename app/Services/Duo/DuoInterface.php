<?php

namespace App\Services\Duo;

use App\Models\User;

interface DuoInterface
{
    public const DUO_SESSION_DOU_USER = 'duo_user';
    public const DUO_SESSION_DUO_STATE = 'duo_state';

    public const DUO_SESSION_DUO_MFA = 'duo_mfa';

    public function setup(User $user): string;

    public function exchangeDuoCodeForUser(User $user, string $duoCode): array;
}
