<?php

namespace App\Services\Duo;

use App\Models\User;
use App\Services\SessionService;
use Duo\DuoUniversal\Client;
use Duo\DuoUniversal\DuoException;
use Illuminate\Support\Facades\Auth;

class DuoService implements DuoInterface
{
    public function __construct(private readonly Client $client)
    {
    }

    /**
     * @param User $user
     * @return string
     * @throws DuoException
     */
    public function setup(User $user): string
    {
        $state = $this->client->generateState();

        SessionService::set(DuoInterface::DUO_SESSION_DOU_USER, $user->email);
        SessionService::set(DuoInterface::DUO_SESSION_DUO_STATE, $state);

        return $this->client->createAuthUrl($user->email, $state);
    }

    /**
     * @throws DuoException
     */
    public function exchangeDuoCodeForUser(User $user, string $duoCode): array
    {
        return $this->client->exchangeAuthorizationCodeFor2FAResult($duoCode, $user->email);
    }

    public function isAuthenticated(): bool
    {
        return Auth::check() && SessionService::has(DuoInterface::DUO_SESSION_DUO_MFA);
    }
}
