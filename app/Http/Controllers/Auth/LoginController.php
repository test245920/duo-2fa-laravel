<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\Duo\DuoInterface;
use App\Services\SessionService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as logoutParent;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DUO_2FA;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(private readonly DuoInterface $duoInterface)
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user): RedirectResponse
    {
        $authPromptUrl = $this->duoInterface->setup($user);

        return redirect()->to($authPromptUrl);
    }

    public function logout(Request $request): RedirectResponse
    {
        SessionService::clean();

        return $this->logoutParent($request);
    }
}
