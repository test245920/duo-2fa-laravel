<?php

namespace App\Http\Controllers;

use App\Services\Duo\DuoInterface;
use App\Services\SessionService;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DuoController extends Controller
{
    public function show(): View
    {
        return view('duo.2fa-form');
    }

    public function verifyUser(Request $request, DuoInterface $duoInterface, UserService $userService): RedirectResponse
    {
        $duoSessionState = SessionService::get(DuoInterface::DUO_SESSION_DUO_STATE);
        $duoSessionEmail = SessionService::get(DuoInterface::DUO_SESSION_DOU_USER);
        if (!$duoSessionState || !$duoSessionEmail) {
            return $this->redirectTo('login', 'No saved state please login again');
        }

        $duoRequestCode = $request->query('duo_code');
        $duoRequestState = $request->query('state');
        if ($duoSessionState != $duoRequestState) {
            return $this->redirectTo('login', 'Duo state does not match saved state');
        }

        $user = $userService->findUserByEmail($duoSessionEmail);
        if (!$user) {
            return $this->redirectTo('login', 'No saved user please login again');
        }

        $duoInterface->exchangeDuoCodeForUser($user, $duoRequestCode);
        SessionService::set(DuoInterface::DUO_SESSION_DUO_MFA, $duoRequestCode);

        return $this->redirectTo('login', 'Login with Duo Security was successful', true);
    }

    private function redirectTo(string $route, string $message = '', bool $success = false): RedirectResponse
    {
        if ($success === false) {
            Auth::logout();
        }

        return redirect()->route($route)->with('status', $message);
    }
}
