<?php

namespace App\Http\Middleware;

use App\Services\Duo\DuoInterface;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    protected DuoInterface $duoInterface;

    public function __construct(Factory $auth)
    {
        $this->duoInterface = app(DuoInterface::class);

        parent::__construct($auth);
    }

    public function handle($request, Closure $next, ...$guards)
    {
        if (!$this->duoInterface->isAuthenticated()) {
            Auth::logout();

            $this->unauthenticated($request, $guards);
        }

        $this->authenticate($request, $guards);

        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('login');
    }
}
