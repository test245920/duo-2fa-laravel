<?php

namespace App\Providers;

use App\Services\Duo\DuoInterface;
use App\Services\Duo\DuoService;
use Duo\DuoUniversal\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(DuoInterface::class, function ($app) {
            return new DuoService($app->make(Client::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        \Illuminate\Database\Eloquent\Model::$snakeAttributes = false;
    }
}
