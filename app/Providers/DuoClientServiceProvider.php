<?php

namespace App\Providers;

use Duo\DuoUniversal\Client;
use Duo\DuoUniversal\DuoException;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class DuoClientServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(Client::class, function (Application $app) {
            $duoClient = new Client(
                $app['config']['duo']['client_id'],
                $app['config']['duo']['client_secret'],
                $app['config']['duo']['api_hostname'],
                $app['config']['duo']['redirect_uri']
            );

            try {
                $duoClient->healthCheck();

                return $duoClient;
            } catch (DuoException $e) {
                Log::error($e->getMessage());
                throw new DuoException('2FA Unavailable');
            }
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
