<?php

namespace App\Traits\Duo;

use App\Models\User;

trait DuoClientSetUp {
    function duoClientSetup(User $user): string
    {
        $state = $this->duoClient->generateState();
        $promptUri = $this->duoClient->createAuthUrl($user->email, $state);

        $user->createSession($state);

        return $promptUri;
    }
}
